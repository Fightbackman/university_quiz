This is a small and specific project for use at the Heinrich Heine University in Germany.
I would love to make this whole project more usable in other environments.
Feel free to contribute but maybe it would help to open an issue with your idea first so we could decide together if it would help the project.